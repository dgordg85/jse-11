package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IAdminEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {
    @NotNull
    public final static String URL = "http://localhost:8080/adminService?wsdl";

    public AdminEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Override
    public void adminBinSave(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().binSave();
    }

    @WebMethod
    @Override
    public void adminBinLoad(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().binLoad();
    }

    @WebMethod
    @Override
    public void adminJsonLoadFasterXML(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonLoadFasterXML();
    }

    @WebMethod
    @Override
    public void adminJsonSaveFasterXML(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonSaveFasterXML();
    }

    @WebMethod
    @Override
    public void adminXmlLoadFasterXML(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlLoadFasterXML();
    }

    @WebMethod
    @Override
    public void adminXmlSaveFasterXML(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlSaveFasterXML();
    }

    @WebMethod
    @Override
    public void adminJsonSaveJaxB(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonSaveJaxB();
    }

    @WebMethod
    @Override
    public void adminJsonLoadJaxB(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonLoadJaxB();
    }

    @WebMethod
    @Override
    public void adminXmlSaveJaxB(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlSaveJaxB();
    }

    @WebMethod
    @Override
    public void adminXmlLoadJaxB(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlLoadJaxB();
    }

    @WebMethod
    @NotNull
    @Override
    public String adminGetDocumentStatusStr(@Nullable final String value) throws Exception {
        return serviceLocator.getAdminService().getDocumentStatusStr(value);
    }

    @WebMethod
    @NotNull
    @Override
    public String adminUserGetRoleTypeStr(@Nullable final String value) throws Exception {
        return serviceLocator.getAdminService().getRoleTypeStr(value);
    }
}
