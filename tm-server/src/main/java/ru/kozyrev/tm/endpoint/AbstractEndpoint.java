package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IAbstractEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.command.AccessForbiddenException;
import ru.kozyrev.tm.exception.session.SessionNotValidException;

@NoArgsConstructor
public abstract class AbstractEndpoint implements IAbstractEndpoint {
    @NotNull
    protected ServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public final void checkSession(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionNotValidException();
        }
        final boolean isSessionValid = serviceLocator.getSessionService().validateSession(session);
        if (!isSessionValid) {
            throw new SessionNotValidException();
        }
    }

    @Override
    public final void checkForbidden(@NotNull final Session session) throws Exception {
        if (!session.getRoleType().equals(RoleType.ADMIN)) {
            throw new AccessForbiddenException();
        }
    }
}
