package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IUserEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {
    @NotNull
    public final static String URL = "http://localhost:8080/userService?wsdl";

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    @Override
    public User userFindOne(
            @Nullable final Session session,
            @Nullable final String userId
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getUserService().findOne(userId);
    }

    @WebMethod
    @NotNull
    @Override
    public User userPersist(@Nullable final User user) throws Exception {
        return serviceLocator.getUserService().persist(user);
    }

    @WebMethod
    @Nullable
    @Override
    public User userMerge(
            @Nullable final Session session,
            @Nullable final User user
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getUserService().merge(user);
    }

    @WebMethod
    @Nullable
    @Override
    public User userRemove(
            @Nullable final Session session,
            @Nullable final String userId
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getUserService().remove(userId);
    }

    @WebMethod
    @Nullable
    @Override
    public User userGetUserByLogin(
            @Nullable final Session session,
            @Nullable final String login
    ) throws Exception {
        checkSession(session);
        return serviceLocator.getUserService().getUserByLogin(login);
    }

    @WebMethod
    @Override
    public void userUpdatePassword(
            @Nullable final Session session,
            @Nullable final String currentPassword,
            @Nullable final String newPassword
    ) throws Exception {
        checkSession(session);
        serviceLocator.getUserService().updateUserPassword(session.getUserId(), currentPassword, newPassword);
    }

    @WebMethod
    @NotNull
    public String userGetUserRoleStr(@Nullable final Session session) throws Exception {
        checkSession(session);
        return serviceLocator.getUserService().getUserRoleStr(session.getUserId());
    }

    @WebMethod
    @Override
    public void userPersistList(
            @Nullable final Session session,
            @Nullable final List<User> users
    ) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getUserService().persist(users);
    }

    @WebMethod
    @Nullable
    @Override
    public List<User> userFindAll(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        return serviceLocator.getUserService().findAll();
    }


    @WebMethod
    @Override
    public void userRemoveAll(@Nullable final Session session) throws Exception {
        checkSession(session);
        checkForbidden(session);
        serviceLocator.getUserService().removeAll();
    }
}
