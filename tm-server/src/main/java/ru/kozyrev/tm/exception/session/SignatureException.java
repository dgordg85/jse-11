package ru.kozyrev.tm.exception.session;

public class SignatureException extends Exception {
    public SignatureException() {
        super("Session can't be signing!");
    }
}
