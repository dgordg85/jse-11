package ru.kozyrev.tm.exception.user;

public class UserEmptyIdException extends Exception {
    public UserEmptyIdException() {
        super("No user id");
    }
}
