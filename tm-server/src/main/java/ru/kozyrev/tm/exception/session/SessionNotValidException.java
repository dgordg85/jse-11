package ru.kozyrev.tm.exception.session;

public class SessionNotValidException extends Exception {
    public SessionNotValidException() {
        super("Session not valid!");
    }
}
