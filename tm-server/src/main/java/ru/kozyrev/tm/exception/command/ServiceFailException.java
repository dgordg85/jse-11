package ru.kozyrev.tm.exception.command;

public class ServiceFailException extends Exception {
    public ServiceFailException() {
        super("One of services is not registry!");
    }
}
