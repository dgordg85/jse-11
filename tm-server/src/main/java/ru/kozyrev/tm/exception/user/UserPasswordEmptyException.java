package ru.kozyrev.tm.exception.user;

public final class UserPasswordEmptyException extends Exception {
    public UserPasswordEmptyException() {
        super("Password is empty!");
    }
}
