package ru.kozyrev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Getter
@RequiredArgsConstructor
public enum SerializeType {
    FASTER_XML("FasterXML"),
    JAX_B("JaxB"),
    JAVA("Java");

    @NotNull
    private final String name;
}