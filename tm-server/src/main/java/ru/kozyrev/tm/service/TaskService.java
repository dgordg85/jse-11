package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.api.service.ITaskService;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.repository.TaskRepository;

import java.util.List;

@NoArgsConstructor
public final class TaskService extends AbstractObjectService<Task> implements ITaskService {
    @NotNull
    private final ITaskRepository taskRepository = (TaskRepository) abstractRepository;

    public TaskService(@NotNull final TaskRepository taskRepository) {
        super(taskRepository);
    }

    @NotNull
    @Override
    public final Task persist(@Nullable final Task task) throws Exception {
        if (task == null) {
            throw new EmptyEntityException();
        }
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new NameException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }
        return taskRepository.persist(task);
    }

    @NotNull
    @Override
    public final Task persist(
            @Nullable final Task task,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (task == null) {
            throw new EmptyEntityException();
        }
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new NameException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }
        return taskRepository.persist(task, userId);
    }

    @Nullable
    @Override
    public final Task merge(
            @Nullable final Task task,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (task == null) {
            throw new EmptyEntityException();
        }
        if (task.getName() == null) {
            throw new NameException();
        }
        if (task.getProjectId() == null) {
            throw new IndexException();
        }
        if (task.getDescription() == null) {
            throw new DescriptionException();
        }
        @Nullable final Task taskUpdate = taskRepository.findOne(task.getId(), userId);
        if (taskUpdate == null) {
            return persist(task, userId);
        }
        if (!task.getName().isEmpty()) {
            taskUpdate.setName(task.getName());
        }
        if (!task.getProjectId().isEmpty()) {
            taskUpdate.setProjectId(task.getProjectId());
        }
        if (!task.getDescription().isEmpty()) {
            taskUpdate.setDescription(task.getDescription());
        }
        if (task.getDateStart() != null) {
            taskUpdate.setDateStart(task.getDateStart());
        }
        if (task.getDateFinish() != null) {
            taskUpdate.setDateFinish(task.getDateFinish());
        }
        if (task.getStatus() != null) {
            taskUpdate.setStatus(task.getStatus());
        }
        return taskRepository.merge(taskUpdate, userId);
    }

    @Override
    public final void removeAll(
            @Nullable final String projectId,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        taskRepository.removeAll(projectId, userId);
    }

    @Override
    public final void removeAllByProjectNum(
            @Nullable final String projectNum,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @NotNull final String projectId = serviceLocator.getProjectService().getEntityIdByShortLink(projectNum, userId);
        removeAll(projectId, userId);

        taskRepository.removeAll(projectId, userId);
    }

    @Nullable
    @Override
    public final List<Task> findAll(
            @Nullable final String projectId,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        return taskRepository.findAll(projectId, userId);
    }

    @Nullable
    @Override
    public final List<Task> findAll(
            @Nullable final String projectId,
            @NotNull final Column column,
            @NotNull final Direction direction,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        @Nullable final List<Task> list;
        switch (column) {
            case DATE_BEGIN:
                list = taskRepository.findAllByDateBegin(projectId, direction, userId);
                break;
            case DATE_FINISH:
                list = taskRepository.findAllByDateFinish(projectId, direction, userId);
                break;
            case STATUS:
                list = taskRepository.findAllByStatus(projectId, direction, userId);
                break;
            default:
                list = taskRepository.findAllByNum(projectId, direction, userId);
        }
        return list;
    }
}
