package ru.kozyrev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ISessionRepository;
import ru.kozyrev.tm.api.service.ISessionService;
import ru.kozyrev.tm.constant.ServerConstant;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.session.SessionCloseException;
import ru.kozyrev.tm.exception.session.SessionNotValidException;
import ru.kozyrev.tm.exception.session.SignatureException;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginNotRegistryException;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;
import ru.kozyrev.tm.repository.SessionRepository;
import ru.kozyrev.tm.util.HashUtil;
import ru.kozyrev.tm.util.SignatureUtil;

public final class SessionService extends AbstractService<Session> implements ISessionService {
    @NotNull
    protected final ISessionRepository sessionRepository = (SessionRepository) abstractRepository;

    public SessionService(@NotNull final SessionRepository sessionRepository) {
        super(sessionRepository);
    }

    @NotNull
    @Override
    public final Session openSession(
            @Nullable final String login,
            @Nullable final String hashPassword
    ) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (hashPassword == null || hashPassword.isEmpty() || hashPassword.equals(HashUtil.EMPTY_PASSWORD)) {
            throw new UserPasswordEmptyException();
        }

        @Nullable final User user = serviceLocator.getUserService().getUserByLogin(login);
        if (user == null) {
            throw new UserLoginNotRegistryException();
        }
        @NotNull final String saltHashPassword = HashUtil.getCycleHash(hashPassword);
        if (!saltHashPassword.equals(user.getPasswordHash())) {
            throw new UserPasswordWrongException();
        }
        @NotNull final Session session = createSession(user);
        return sessionRepository.persist(session);
    }

    @Override
    public final boolean validateSession(@Nullable final Session session) {
        return isSessionExist(session) && isSessionAlive(session);
    }

    private final boolean isSessionExist(@Nullable final Session session) {
        if (session == null) {
            return false;
        }
        @Nullable final Session serverSession = sessionRepository.findOne(session.getId());
        if (serverSession == null) {
            return false;
        }
        return serverSession.equals(session);
    }

    private final boolean isSessionAlive(@Nullable final Session session) {
        if (session == null) {
            return false;
        }
        final long sessionTime = System.currentTimeMillis() - session.getTimestamp();
        if (ServerConstant.SESSION_LIFETIME < sessionTime) {
            sessionRepository.remove(session.getId());
            return false;
        }
        return true;
    }

    @Override
    public final void closeSession(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionCloseException();
        }
        if (!isSessionExist(session)) {
            throw new SessionNotValidException();
        }
        sessionRepository.remove(session.getId());
    }

    @NotNull
    private final Session createSession(@NotNull final User user) throws Exception {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRoleType(user.getRoleType());
        session.setTimestamp(System.currentTimeMillis());

        @Nullable final String signature = SignatureUtil.sign(session);
        if (signature == null) {
            throw new SignatureException();
        }
        session.setSignature(signature);
        return session;
    }
}
