package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IAbstractObjectRepository;
import ru.kozyrev.tm.api.service.IAbstractObjectService;
import ru.kozyrev.tm.entity.AbstractObject;
import ru.kozyrev.tm.exception.data.NoDataException;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.repository.AbstractObjectRepository;
import ru.kozyrev.tm.util.StringUtil;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractObjectService<T extends AbstractObject> extends AbstractService<T> implements IAbstractObjectService<T> {
    @NotNull
    protected final IAbstractObjectRepository<T> abstractObjectRepository = (IAbstractObjectRepository<T>) abstractRepository;

    public AbstractObjectService(@NotNull final AbstractObjectRepository<T> abstractObjectRepository) {
        super(abstractObjectRepository);
    }

    @Nullable
    @Override
    public final T findOne(
            @Nullable final String id,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (id == null || id.isEmpty()) {
            return null;
        }
        return abstractObjectRepository.findOne(id, userId);
    }

    @Nullable
    @Override
    public final List<T> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        return abstractObjectRepository.findAll(userId);
    }

    @NotNull
    @Override
    public abstract T persist(@Nullable final T object) throws Exception;

    @NotNull
    @Override
    public abstract T persist(@Nullable final T object, @Nullable final String userId) throws Exception;

    @Nullable
    @Override
    public abstract T merge(@Nullable final T object, @Nullable final String userId) throws Exception;

    @Nullable
    @Override
    public final T remove(@Nullable final String objectId, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (objectId == null || objectId.isEmpty()) {
            return null;
        }
        return abstractObjectRepository.remove(objectId, userId);
    }

    @Override
    public final void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        abstractObjectRepository.removeAll(userId);
    }

    @Nullable
    @Override
    public List<T> findWord(@NotNull final String word, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        return abstractObjectRepository.findWord(word, userId);
    }

    @Override
    public final void persist(@Nullable final String userId, @Nullable final List<T> objects) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (objects == null) {
            throw new NoDataException();
        }
        for (T object : objects) {
            persist(object, userId);
        }
    }

    @NotNull
    @Override
    public final String getEntityIdByShortLink(@Nullable final String num, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }
        final int shortLink = StringUtil.parseToInt(num);
        @NotNull final T entity = abstractObjectRepository.findOneByShortLink(shortLink, userId);
        return entity.getId();
    }

    @Nullable
    @Override
    public T removeEntityByShortLink(@Nullable final String num, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @NotNull final String entityId = getEntityIdByShortLink(num, userId);
        return abstractObjectRepository.remove(entityId, userId);
    }
}
