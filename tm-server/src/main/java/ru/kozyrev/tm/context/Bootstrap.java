package ru.kozyrev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.*;
import ru.kozyrev.tm.api.service.*;
import ru.kozyrev.tm.endpoint.*;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.repository.SessionRepository;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.service.*;
import ru.kozyrev.tm.util.HashUtil;

import javax.xml.ws.Endpoint;


@Getter
public final class Bootstrap implements ServiceLocator {
    @NotNull
    ISessionEndpoint sessionEndpoint;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IAdminService adminService;

    @NotNull
    private IProjectEndpoint projectEndpoint;

    @NotNull
    private ITaskEndpoint taskEndpoint;

    @NotNull
    private IUserEndpoint userEndpoint;

    @NotNull
    private IAdminEndpoint adminEndpoint;


    public void start() throws Exception {
        initServices();
        initEndpoint();
        createDefaultUsers();
        publicEndpoints();
    }

    public final void createDefaultUsers() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPasswordHash(HashUtil.getHash("user"));
        user.setRoleType(RoleType.USER);
        userService.persist(user);

        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordHash(HashUtil.getHash("admin"));
        admin.setRoleType(RoleType.ADMIN);
        userService.persist(admin);
    }

    private final void initEndpoint() {
        projectEndpoint = new ProjectEndpoint(this);
        taskEndpoint = new TaskEndpoint(this);
        userEndpoint = new UserEndpoint(this);
        adminEndpoint = new AdminEndpoint(this);
        sessionEndpoint = new SessionEndpoint(this);
    }

    private final void initServices() {
        projectService = new ProjectService(new ProjectRepository());
        projectService.setServiceLocator(this);

        taskService = new TaskService(new TaskRepository());
        taskService.setServiceLocator(this);

        userService = new UserService(new UserRepository());

        sessionService = new SessionService(new SessionRepository());
        sessionService.setServiceLocator(this);

        adminService = new AdminService();
        adminService.setServiceLocator(this);
    }

    private final void publicEndpoints() {
        publicAddress(ProjectEndpoint.URL, (ProjectEndpoint) projectEndpoint);
        publicAddress(TaskEndpoint.URL, (TaskEndpoint) taskEndpoint);
        publicAddress(UserEndpoint.URL, (UserEndpoint) userEndpoint);
        publicAddress(AdminEndpoint.URL, (AdminEndpoint) adminEndpoint);
        publicAddress(SessionEndpoint.URL, (SessionEndpoint) sessionEndpoint);
    }

    private final void publicAddress(@NotNull final String url, @NotNull final AbstractEndpoint endpoint) {
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }
}
