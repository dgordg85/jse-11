package ru.kozyrev.tm.comporator;

import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.AbstractObject;

import java.util.Comparator;
import java.util.Date;

public final class DateStartComparator implements Comparator<AbstractObject> {
    public final int compare(AbstractObject o1, AbstractObject o2) {
        @Nullable final Date dateStart = o1.getDateStart();
        if (dateStart == null) {
            return 1;
        }
        return o1.getDateStart().compareTo(o2.getDateStart());
    }
}