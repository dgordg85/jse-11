package ru.kozyrev.tm.comporator;

import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.AbstractObject;

import java.util.Comparator;
import java.util.Date;

public final class DateFinishComparator implements Comparator<AbstractObject> {
    public final int compare(AbstractObject o1, AbstractObject o2) {
        @Nullable final Date dateFinish = o1.getDateFinish();
        if (dateFinish == null) {
            return 1;
        }
        return o1.getDateFinish().compareTo(o2.getDateFinish());
    }
}
