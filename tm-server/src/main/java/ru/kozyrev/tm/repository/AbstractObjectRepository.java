package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IAbstractObjectRepository;
import ru.kozyrev.tm.entity.AbstractObject;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractObjectRepository<T extends AbstractObject> extends AbstractRepository<T> implements IAbstractObjectRepository<T> {
    @Nullable
    @Override
    public final T findOne(
            @NotNull final String id,
            @NotNull final String userId
    ) {
        @Nullable final T object = findOne(id);
        if (object == null || object.getUserId() == null) {
            return null;
        }
        return object.getUserId().equals(userId) ? object : null;
    }

    @Nullable
    @Override
    public final List<T> findAll(@NotNull final String userId) {
        @NotNull final List<T> list = new ArrayList<>();
        @NotNull final List<T> allEntities = findAll();
        if (allEntities.size() == 0) {
            return null;
        }
        for (T object : allEntities) {
            if (object.getUserId() == null) {
                continue;
            }
            if (object.getUserId().equals(userId)) {
                list.add(object);
            }
        }
        if (list.size() == 0) {
            return null;
        }
        return list;
    }

    @NotNull
    @Override
    public final T persist(
            @NotNull final T object,
            @NotNull final String userId
    ) throws Exception {
        if (map.containsKey(object.getId())) {
            throw new EntityException();
        }
        object.setShortLink(count++);
        object.setUserId(userId);
        if (object.getStatus() == null) {
            object.setStatus(DocumentStatus.PLANNED);
        }
        persist(object);
        return object;
    }

    @Nullable
    @Override
    public final T merge(
            @NotNull final T object,
            @NotNull final String userId
    ) {
        @Nullable final T entityUpdate = findOne(object.getId(), userId);
        if (entityUpdate == null) {
            return null;
        }
        merge(object);
        return object;
    }

    @Nullable
    @Override
    public final T remove(
            @NotNull final String id,
            @NotNull final String userId
    ) throws Exception {
        @Nullable final T entity = findOne(id);
        if (entity == null) {
            return null;
        }
        @Nullable final String entityUserId = entity.getUserId();
        if (entityUserId == null) {
            throw new UserEmptyIdException();
        }
        if (entityUserId.equals(userId)) {
            return remove(id);
        }
        return null;
    }

    @Override
    public final void removeAll(@NotNull final String userId) {
        @Nullable final List<T> allEntities = findAll(userId);
        if (allEntities != null) {
            for (T entity : allEntities) {
                remove(entity.getId());
            }
        }
    }

    @NotNull
    @Override
    public final T findOneByShortLink(
            @NotNull final Integer shortLink,
            @NotNull final String userId
    ) throws Exception {
        @Nullable final List<T> allEntities = findAll(userId);
        if (allEntities == null) {
            throw new IndexException();
        }
        for (T entity : allEntities) {
            if (entity.getShortLink() == null || entity.getUserId() == null) {
                continue;
            }
            if (entity.getShortLink().equals(shortLink)) {
                return entity;
            }
        }
        throw new IndexException();
    }

    @Nullable
    @Override
    public final List<T> findWord(
            @NotNull final String word,
            @NotNull final String userId
    ) {
        @Nullable final List<T> list = findAll(userId);
        @NotNull final List<T> result = new ArrayList<>();
        if (list == null || list.size() == 0) {
            return null;
        }
        for (T object : list) {
            @Nullable final String objectName = object.getName();
            if (objectName != null) {
                if (objectName.indexOf(word) > 0) {
                    result.add(object);
                    continue;
                }
            }
            @Nullable final String objectDescription = object.getDescription();
            if (objectDescription != null) {
                if (object.getDescription().indexOf(word) > 0) {
                    result.add(object);
                }
            }
        }
        if (result.size() == 0) {
            return null;
        }
        return result;
    }
}
