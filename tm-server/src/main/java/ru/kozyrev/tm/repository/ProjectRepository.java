package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IProjectRepository;
import ru.kozyrev.tm.comporator.DateFinishComparator;
import ru.kozyrev.tm.comporator.DateStartComparator;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractObjectRepository<Project> implements IProjectRepository {
    @Override
    public final List<Project> findAllByNum(
            @NotNull final Direction direction,
            @NotNull final String userId
    ) {
        @Nullable final List<Project> list = findAll(userId);
        if (list == null) {
            return null;
        }
        if (direction == Direction.DSC) {
            Collections.reverse(list);
        }
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllByDateBegin(
            @NotNull final Direction direction,
            @NotNull final String userId
    ) {
        @Nullable final List<Project> list = findAll(userId);
        if (list == null) {
            return null;
        }
        list.sort(new DateStartComparator());
        if (direction == Direction.DSC) {
            Collections.reverse(list);
        }
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllByDateFinish(
            @NotNull final Direction direction,
            @NotNull final String userId
    ) {
        @Nullable final List<Project> list = findAll(userId);
        if (list == null) {
            return null;
        }
        list.sort(new DateFinishComparator());
        if (direction == Direction.DSC) {
            Collections.reverse(list);
        }
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllByStatus(
            @NotNull final Direction direction,
            @NotNull final String userId
    ) {
        @Nullable final List<Project> list = findAll(userId);
        if (list == null) {
            return null;
        }
        list.sort(Comparator.comparing(o -> o.getStatus().getSortId()));
        if (direction == Direction.DSC) {
            Collections.reverse(list);
        }
        return list;
    }
}
