package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.entity.EntityException;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @Nullable
    @Override
    public final User getUserByLogin(@NotNull final String login) {
        @NotNull final List<User> list = findAll();
        if (list.size() == 0) {
            return null;
        }
        @Nullable String userLogin;
        for (User user : list) {
            userLogin = user.getLogin();
            if (userLogin == null) {
                continue;
            }
            if (userLogin.equals(login)) {
                return user;
            }
        }
        return null;
    }

    @NotNull
    @Override
    public final User persist(@NotNull final User user) throws Exception {
        if (map.containsKey(user.getId())) {
            throw new EntityException();
        }
        map.put(user.getId(), user);
        return user;
    }
}
