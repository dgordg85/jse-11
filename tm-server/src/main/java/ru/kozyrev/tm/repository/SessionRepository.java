package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.api.repository.ISessionRepository;
import ru.kozyrev.tm.entity.Session;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
}
