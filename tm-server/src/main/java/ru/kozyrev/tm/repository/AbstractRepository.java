package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IRepository;
import ru.kozyrev.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {
    @NotNull
    protected final Map<String, T> map = new LinkedHashMap<>();
    protected int count = 1;

    @Nullable
    @Override
    public final T findOne(@NotNull final String id) {
        return map.get(id);
    }

    @NotNull
    @Override
    public final List<T> findAll() {
        return new ArrayList<>(map.values());
    }

    @NotNull
    @Override
    public T persist(@NotNull final T entity) throws Exception {
        map.put(entity.getId(), entity);
        return entity;
    }

    @NotNull
    @Override
    public final T merge(@NotNull final T entity) {
        map.put(entity.getId(), entity);
        return entity;
    }

    @Nullable
    @Override
    public final T remove(@NotNull final String id) {
        return map.remove(id);
    }

    @Override
    public final void removeAll() {
        map.clear();
        count = 1;
    }
}
