package ru.kozyrev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.comporator.DateFinishComparator;
import ru.kozyrev.tm.comporator.DateStartComparator;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractObjectRepository<Task> implements ITaskRepository {
    @Nullable
    @Override
    public final List<Task> findAll(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        @Nullable final List<Task> taskList = findAll(userId);
        if (taskList == null) {
            return null;
        }
        @NotNull final List<Task> result = new ArrayList<>();
        @Nullable String currentProjectId;
        for (Task task : taskList) {
            currentProjectId = task.getProjectId();
            if (currentProjectId == null) {
                continue;
            }
            if (currentProjectId.equals(projectId)) {
                result.add(task);
            }
        }
        return result;
    }

    @Override
    public final void removeAll(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        @Nullable final List<Task> list = findAll(userId);
        if (list == null) {
            return;
        }
        @Nullable String currentProjectId;
        for (Task task : list) {
            currentProjectId = task.getProjectId();
            if (currentProjectId == null) {
                continue;
            }
            if (currentProjectId.equals(projectId)) {
                remove(task.getId());
            }
        }
    }

    @Nullable
    @Override
    public final List<Task> findAllByNum(
            @NotNull final String projectId,
            @NotNull final Direction direction,
            @NotNull final String userId
    ) {
        @Nullable final List<Task> list = findAll(projectId, userId);
        if (list == null) {
            return null;
        }
        if (direction == Direction.DSC) {
            Collections.reverse(list);
        }
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllByDateBegin(
            @NotNull final String projectId,
            @NotNull final Direction direction,
            @NotNull final String userId
    ) {
        @Nullable final List<Task> list = findAll(projectId, userId);
        if (list == null) {
            return null;
        }
        list.sort(new DateStartComparator());
        if (direction == Direction.DSC) {
            Collections.reverse(list);
        }
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllByDateFinish(
            @NotNull final String projectId,
            @NotNull final Direction direction,
            @NotNull final String userId
    ) {
        @Nullable final List<Task> list = findAll(projectId, userId);
        if (list == null) {
            return null;
        }
        list.sort(new DateFinishComparator());
        if (direction == Direction.DSC) {
            Collections.reverse(list);
        }
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllByStatus(
            @NotNull final String projectId,
            @NotNull final Direction direction,
            @NotNull final String userId
    ) {
        @Nullable final List<Task> list = findAll(projectId, userId);
        if (list == null) {
            return null;
        }
        list.sort(Comparator.comparing(o -> o.getStatus().getSortId()));
        if (direction == Direction.DSC) {
            Collections.reverse(list);
        }
        return list;
    }
}
