package ru.kozyrev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.exception.entity.DateException;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {
    @NotNull
    private final static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    @NotNull
    public final static String getDate(@Nullable final Date date) {
        return formatter.format(date);
    }

    @NotNull
    public final static Date parseDate(@Nullable final String date) throws Exception {
        if (date == null || date.isEmpty()) {
            throw new DateException();
        }
        return formatter.parse(date);
    }
}
