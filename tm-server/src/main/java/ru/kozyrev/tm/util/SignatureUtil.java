package ru.kozyrev.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.ServerConstant;
import ru.kozyrev.tm.exception.session.SignatureException;

public final class SignatureUtil {
    @Nullable
    public static String sign(@Nullable final Object value) throws SignatureException {
        return sign(value, ServerConstant.SALT, ServerConstant.CYCLE);
    }

    @Nullable
    public static String sign(
            @Nullable final Object value,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) throws SignatureException {
        try {
            @NotNull final ObjectMapper objectMapper =
                    new ObjectMapper();
            @NotNull final String json =
                    objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @NotNull
    public static String sign(
            @Nullable final String value,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) throws SignatureException {
        if (value == null || salt == null || cycle == null) {
            throw new SignatureException();
        }
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.getHash(salt + result + salt);
        }
        return result;
    }
}
