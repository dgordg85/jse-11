package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {
    @Nullable
    User findOne(@NotNull String userId);

    @NotNull
    List<User> findAll();

    @NotNull
    User persist(@NotNull User user) throws Exception;

    @NotNull
    User merge(@NotNull User user);

    @Nullable
    User remove(@NotNull String userId);

    void removeAll();

    @Nullable
    User getUserByLogin(@NotNull String login);
}