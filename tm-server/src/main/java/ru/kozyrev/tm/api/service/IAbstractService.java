package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IAbstractService<T> {
    @Nullable
    T findOne(@Nullable String id) throws Exception;

    @Nullable
    List<T> findAll() throws Exception;

    @NotNull
    T persist(@Nullable T object) throws Exception;

    @Nullable
    T merge(@Nullable T object) throws Exception;

    @Nullable
    T remove(@Nullable String entityId) throws Exception;

    void removeAll() throws Exception;

    void persist(@Nullable List<T> objects) throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);
}
