package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.List;

public interface IProjectEndpoint extends IAbstractEndpoint {
    @Nullable
    Project projectFindOneAuth(@Nullable Session session, @Nullable String id) throws Exception;

    @Nullable
    List<Project> projectFindAllAuth(@Nullable Session session) throws Exception;

    @Nullable
    List<Project> projectFindAll(@Nullable Session session) throws Exception;

    @Nullable
    Project projectRemoveAuth(@Nullable Session session, @Nullable String projectId) throws Exception;

    void projectRemoveAllAuth(@Nullable Session session) throws Exception;

    void projectRemoveAll(@Nullable Session session) throws Exception;

    @NotNull
    String projectGetEntityIdByShortLink(@Nullable Session session, @Nullable String num) throws Exception;

    @Nullable
    List<Project> projectFindWord(@Nullable Session session, @NotNull String word) throws Exception;

    void projectPersistListAuth(@Nullable Session session, @Nullable List<Project> projects) throws Exception;

    void projectPersistList(@Nullable Session session, @Nullable List<Project> projects) throws Exception;

    @NotNull
    Project projectPersistAuth(@Nullable Session session, @Nullable Project project) throws Exception;

    @Nullable
    Project projectMergeAuth(@Nullable Session session, @Nullable Project project) throws Exception;

    @Nullable
    List<Project> projectFindAllBySorting(@Nullable Session session, @NotNull Column column, @NotNull Direction direction) throws Exception;

    Project projectRemoveByShortLink(@Nullable Session session, @Nullable String num) throws Exception;
}
