package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;

import java.util.List;

public interface ISessionService {
    @Nullable
    Session findOne(@Nullable String sessionId) throws Exception;

    @Nullable
    List<Session> findAll() throws Exception;

    @NotNull
    Session persist(@Nullable Session session) throws Exception;

    @Nullable
    Session merge(@Nullable Session session) throws Exception;

    @Nullable
    Session remove(@Nullable String sessionId) throws Exception;

    void removeAll() throws Exception;

    void persist(@Nullable List<Session> sessions) throws Exception;

    @NotNull
    Session openSession(@Nullable String login, @Nullable String hashPassword) throws Exception;

    boolean validateSession(@Nullable Session session);

    void closeSession(@Nullable Session session) throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);
}
