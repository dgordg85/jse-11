package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserService extends IAbstractService<User> {
    @Nullable
    User findOne(@Nullable String userId) throws Exception;

    @Nullable
    List<User> findAll() throws Exception;

    @Nullable
    User remove(@Nullable String userId) throws Exception;

    void removeAll() throws Exception;

    @NotNull
    User persist(@Nullable User user) throws Exception;

    @Nullable
    User merge(@Nullable User user) throws Exception;

    @NotNull
    User persistFromList(@Nullable User user) throws Exception;

    @Nullable
    User getUserByLogin(@Nullable String login) throws Exception;

    void updateUserPassword(@Nullable String userId, @Nullable String currentPassword, @Nullable String hashPassword) throws Exception;

    boolean isPasswordTrue(@Nullable String userLogin, @Nullable String hashPassword) throws Exception;

    @NotNull
    String getUserRoleStr(@Nullable String userId) throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);
}
