package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.List;

public interface IProjectRepository extends IAbstractObjectRepository<Project> {
    @Nullable
    Project findOne(@NotNull String projectId);

    @NotNull
    List<Project> findAll();

    @NotNull
    Project persist(@NotNull Project project) throws Exception;

    @NotNull
    Project merge(@NotNull Project project);

    @Nullable
    Project remove(@NotNull String projectId);

    void removeAll();

    @Nullable
    Project findOne(@NotNull String projectId, @NotNull String userId) throws Exception;

    @Nullable
    List<Project> findAll(@NotNull String userId) throws Exception;

    @NotNull
    Project persist(@NotNull Project project, @NotNull String userId) throws Exception;

    @Nullable
    Project merge(@NotNull Project entity, @NotNull String userId) throws Exception;

    @Nullable
    Project remove(@NotNull String projectId, @NotNull String userId) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    @Nullable
    List<Project> findWord(@NotNull String word, @NotNull String userId);

    @NotNull
    Project findOneByShortLink(@NotNull Integer shortLink, @NotNull String userId) throws Exception;

    @Nullable
    List<Project> findAllByNum(Direction direction, String userId);

    @Nullable
    List<Project> findAllByDateBegin(Direction direction, String userId);

    @Nullable
    List<Project> findAllByDateFinish(Direction direction, String userId);

    @Nullable
    List<Project> findAllByStatus(Direction direction, String userId);
}
