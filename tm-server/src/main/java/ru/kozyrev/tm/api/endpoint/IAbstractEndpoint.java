package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;

public interface IAbstractEndpoint {
    void checkSession(@Nullable final Session session) throws Exception;

    void checkForbidden(@NotNull final Session session) throws Exception;
}
