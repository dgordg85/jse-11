package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {
    @Nullable
    Session findOne(@NotNull String sessionId);

    @NotNull
    List<Session> findAll();

    @NotNull
    Session persist(@NotNull Session session) throws Exception;

    @NotNull
    Session merge(@NotNull Session session);

    @Nullable
    Session remove(@NotNull String sessionId);

    void removeAll();
}
