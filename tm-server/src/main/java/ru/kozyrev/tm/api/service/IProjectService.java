package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.List;

public interface IProjectService extends IAbstractObjectService<Project> {
    @Nullable
    Project findOne(@Nullable String projectId) throws Exception;

    @Nullable
    List<Project> findAll() throws Exception;

    @Nullable
    Project merge(@Nullable Project project) throws Exception;

    @Nullable
    Project remove(@Nullable String projectId) throws Exception;

    void removeAll() throws Exception;

    void persist(@Nullable List<Project> projects) throws Exception;

    @Nullable
    Project findOne(@Nullable String projectId, @Nullable String userId) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String userId) throws Exception;

    @Nullable
    Project remove(@Nullable String projectId, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    String getEntityIdByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    @Nullable
    List<Project> findWord(@NotNull String word, @Nullable String userId) throws Exception;

    @NotNull
    Project persist(@Nullable Project project) throws Exception;

    @NotNull
    Project persist(@Nullable Project project, @Nullable String userId) throws Exception;

    @Nullable
    Project merge(@Nullable Project project, @Nullable String userId) throws Exception;

    @Nullable
    List<Project> findAll(@NotNull Column column, @NotNull Direction direction, @Nullable String userId);

    @Nullable
    Project removeEntityByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);
}
