package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;

public interface ISessionEndpoint extends IAbstractEndpoint {
    @NotNull
    Session openSession(@Nullable String login, @Nullable String hashPassword) throws Exception;

    void closeSession(@Nullable Session session) throws Exception;
}
