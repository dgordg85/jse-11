package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;

public interface IAdminEndpoint extends IAbstractEndpoint {
    void adminBinSave(@Nullable Session session) throws Exception;

    void adminBinLoad(@Nullable Session session) throws Exception;

    void adminJsonLoadFasterXML(@Nullable Session session) throws Exception;

    void adminJsonSaveFasterXML(@Nullable Session session) throws Exception;

    void adminXmlLoadFasterXML(@Nullable Session session) throws Exception;

    void adminXmlSaveFasterXML(@Nullable Session session) throws Exception;

    void adminJsonSaveJaxB(@Nullable Session session) throws Exception;

    void adminJsonLoadJaxB(@Nullable Session session) throws Exception;

    void adminXmlSaveJaxB(@Nullable Session session) throws Exception;

    void adminXmlLoadJaxB(@Nullable Session session) throws Exception;

    @NotNull
    String adminGetDocumentStatusStr(@Nullable String value) throws Exception;

    @NotNull
    String adminUserGetRoleTypeStr(@Nullable String value) throws Exception;
}
