package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserEndpoint extends IAbstractEndpoint {
    @Nullable
    User userFindOne(@Nullable Session session, @Nullable String userId) throws Exception;

    @Nullable
    User userMerge(@Nullable Session session, @Nullable User user) throws Exception;

    @Nullable
    User userRemove(@Nullable final Session session, @Nullable final String userId) throws Exception;

    @NotNull
    User userPersist(@Nullable User user) throws Exception;

    @Nullable
    User userGetUserByLogin(@Nullable Session session, @Nullable String login) throws Exception;

    void userUpdatePassword(@Nullable Session session, @Nullable String currentPassword, @Nullable String hashPassword) throws Exception;

    @NotNull
    String userGetUserRoleStr(@Nullable Session session) throws Exception;

    void userRemoveAll(@Nullable Session session) throws Exception;

    @Nullable
    List<User> userFindAll(@Nullable Session session) throws Exception;

    void userPersistList(@Nullable Session session, @Nullable List<User> users) throws Exception;
}
