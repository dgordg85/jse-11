package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.List;

public interface ITaskRepository extends IAbstractObjectRepository<Task> {
    @Nullable
    Task findOne(@NotNull String taskId);

    @NotNull
    List<Task> findAll();

    @NotNull
    Task persist(@NotNull Task task) throws Exception;

    @NotNull
    Task merge(@NotNull Task task);

    @Nullable
    Task remove(@NotNull String taskId);

    void removeAll();

    @Nullable
    Task findOne(@NotNull String taskId, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAll(@NotNull String userId) throws Exception;

    @NotNull
    Task persist(@NotNull Task task, @NotNull String userId) throws Exception;

    @Nullable
    Task merge(@NotNull Task task, @NotNull String userId) throws Exception;

    @Nullable
    Task remove(@NotNull String taskId, @NotNull String userId) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    @Nullable
    List<Task> findWord(@NotNull String word, @NotNull String userId);

    @NotNull
    Task findOneByShortLink(@NotNull Integer shortLink, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAll(@NotNull String projectId, @NotNull String userId);

    void removeAll(@NotNull String projectId, @NotNull String userId);

    @Nullable
    List<Task> findAllByNum(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId);

    @Nullable
    List<Task> findAllByDateBegin(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId);

    @Nullable
    List<Task> findAllByDateFinish(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId);

    @Nullable
    List<Task> findAllByStatus(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId);
}
