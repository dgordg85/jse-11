package ru.kozyrev.tm.exception.user;

public class UserUpdateErrorException extends Exception {
    public UserUpdateErrorException() {
        super("Not Update! Check password and new login!");
    }
}
