package ru.kozyrev.tm.exception.enumerated;

public class UnknownTypeException extends Exception {
    public UnknownTypeException() {
        super("Unknown sort column!");
    }
}
