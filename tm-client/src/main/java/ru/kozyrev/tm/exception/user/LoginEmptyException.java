package ru.kozyrev.tm.exception.user;

public class LoginEmptyException extends Exception {
    public LoginEmptyException() {
        super("Login empty!");
    }
}
