package ru.kozyrev.tm.exception.entity;

public class EmptyStringException extends Exception {
    public EmptyStringException() {
        super("Empty string!");
    }
}
