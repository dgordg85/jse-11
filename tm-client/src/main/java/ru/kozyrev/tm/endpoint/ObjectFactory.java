
package ru.kozyrev.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.kozyrev.tm.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.kozyrev.ru/", "Exception");
    private final static QName _CloseSession_QNAME = new QName("http://endpoint.tm.kozyrev.ru/", "closeSession");
    private final static QName _CloseSessionResponse_QNAME = new QName("http://endpoint.tm.kozyrev.ru/", "closeSessionResponse");
    private final static QName _GetURL_QNAME = new QName("http://endpoint.tm.kozyrev.ru/", "getURL");
    private final static QName _GetURLResponse_QNAME = new QName("http://endpoint.tm.kozyrev.ru/", "getURLResponse");
    private final static QName _OpenSession_QNAME = new QName("http://endpoint.tm.kozyrev.ru/", "openSession");
    private final static QName _OpenSessionResponse_QNAME = new QName("http://endpoint.tm.kozyrev.ru/", "openSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.kozyrev.tm.endpoint
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     *
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CloseSession }
     *
     */
    public CloseSession createCloseSession() {
        return new CloseSession();
    }

    /**
     * Create an instance of {@link CloseSessionResponse }
     *
     */
    public CloseSessionResponse createCloseSessionResponse() {
        return new CloseSessionResponse();
    }

    /**
     * Create an instance of {@link GetURL }
     *
     */
    public GetURL createGetURL() {
        return new GetURL();
    }

    /**
     * Create an instance of {@link GetURLResponse }
     *
     */
    public GetURLResponse createGetURLResponse() {
        return new GetURLResponse();
    }

    /**
     * Create an instance of {@link OpenSession }
     *
     */
    public OpenSession createOpenSession() {
        return new OpenSession();
    }

    /**
     * Create an instance of {@link OpenSessionResponse }
     *
     */
    public OpenSessionResponse createOpenSessionResponse() {
        return new OpenSessionResponse();
    }

    /**
     * Create an instance of {@link Session }
     *
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kozyrev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kozyrev.ru/", name = "closeSession")
    public JAXBElement<CloseSession> createCloseSession(CloseSession value) {
        return new JAXBElement<CloseSession>(_CloseSession_QNAME, CloseSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kozyrev.ru/", name = "closeSessionResponse")
    public JAXBElement<CloseSessionResponse> createCloseSessionResponse(CloseSessionResponse value) {
        return new JAXBElement<CloseSessionResponse>(_CloseSessionResponse_QNAME, CloseSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetURL }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kozyrev.ru/", name = "getURL")
    public JAXBElement<GetURL> createGetURL(GetURL value) {
        return new JAXBElement<GetURL>(_GetURL_QNAME, GetURL.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetURLResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kozyrev.ru/", name = "getURLResponse")
    public JAXBElement<GetURLResponse> createGetURLResponse(GetURLResponse value) {
        return new JAXBElement<GetURLResponse>(_GetURLResponse_QNAME, GetURLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kozyrev.ru/", name = "openSession")
    public JAXBElement<OpenSession> createOpenSession(OpenSession value) {
        return new JAXBElement<OpenSession>(_OpenSession_QNAME, OpenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kozyrev.ru/", name = "openSessionResponse")
    public JAXBElement<OpenSessionResponse> createOpenSessionResponse(OpenSessionResponse value) {
        return new JAXBElement<OpenSessionResponse>(_OpenSessionResponse_QNAME, OpenSessionResponse.class, null, value);
    }

}
