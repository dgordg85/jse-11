package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.endpoint.*;

public interface EndpointLocator {
    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();
}
