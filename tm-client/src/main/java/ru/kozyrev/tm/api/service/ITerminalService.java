package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;

import java.util.Scanner;

public interface ITerminalService {
    void printCommands() throws Exception;

    void registryCommand(@Nullable AbstractCommand command) throws Exception;

    void execute(@Nullable String commandStr) throws Exception;

    boolean isCommandAllow(@Nullable AbstractCommand command) throws Exception;

    void initCommands() throws Exception;

    void welcomeMsg();

    void errorDateMsg();

    @NotNull
    String nextLine();

    void setSc(@NotNull Scanner sc);

    void closeSc();

    void setEndpointLocator(@NotNull EndpointLocator endpointLocator);

    void setStateService(@NotNull IStateService stateService);
}
