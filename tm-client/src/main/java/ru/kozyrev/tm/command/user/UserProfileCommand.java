package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.endpoint.RoleType;
import ru.kozyrev.tm.endpoint.Session;

public final class UserProfileCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 7;

    public UserProfileCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show user profile";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final Session session = stateService.getSession();

        System.out.println("[USER PROFILE]");
        System.out.printf("Login: %s\n", userEndpoint.userFindOne(session, session.getUserId()).getLogin());
        System.out.printf("Role: %s\n", userEndpoint.userGetUserRoleStr(session));
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
